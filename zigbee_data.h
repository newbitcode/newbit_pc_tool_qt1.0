#ifndef ZIGBEE_DATA_H
#define ZIGBEE_DATA_H

#include <QWidget>


#define NEWBIT_TOOL_CMD_INDEX           0
#define NEWBIT_TOOL_CMD_LENGTH          2
/*******************************************/
#define TPHP_PAN_ID_INDEX             2
#define TPHP_PAN_ID_LENGTH            2
#define TPHP_MAC_ADDR_INDEX           4
#define TPHP_MAC_ADDR_LENGTH          8
#define TPHP_SHORT_ADDR_INDEX         12
#define TPHP_SHORT_ADDR_LENGTH        2
#define TPHP_HUMI_DATA_INDEX          14
#define TPHP_HUMI_DATA_LENGTH         1
#define TPHP_TEMP_DATA_INDEX          15
#define TPHP_TEMP_DATA_LENGTH         1
#define TPHP_CHECK_SUM_INDEX          16
#define TPHP_CHECK_SUM_LENGTH         1

#define TPHP_CMD_DATA_LENGTH          17
/*******************************************/
#define TOPOLOGY_PAN_ID_INDEX             2
#define TOPOLOGY_PAN_ID_LENGTH            2
#define TOPOLOGY_MAC_ADDR_INDEX           4
#define TOPOLOGY_MAC_ADDR_LENGTH          8
#define TOPOLOGY_PARENT_ADDR_INDEX        12
#define TOPOLOGY_PARENT_ADDR_LENGTH       2
#define TOPOLOGY_SHORT_ADDR_INDEX         14
#define TOPOLOGY_SHORT_ADDR_LENGTH        2
#define TOPOLOGY_DEV_TYPE_INDEX           16
#define TOPOLOGY_DEV_TYPE_LENGTH          1
#define TOPOLOGY_DEPTH_INDEX              17
#define TOPOLOGY_DEPTH_LENGTH             1
#define TOPOLOGY_CHECK_SUM_INDEX          18
#define TOPOLOGY_CHECK_SUM_LENGTH         1

#define TOPOLOGY_CMD_DATA_LENGTH          19
/*********************************************/
enum NewbitToolCmd_e
{
    NEWBIT_TOOL_CMD_TOPOLOGY        = 0x0001,
    NEWBIT_TOOL_CMD_TPHP            = 0x0002,

    NEWBIT_TOOL_CMD_NUM
};

enum ZIGBEE_DEV_TYPE
{
   ZIGBEE_DEV_COOR = 0,
   ZIGBEE_DEV_ROUTER = 1,
   ZIGBEE_DEV_ENDDEVICE = 2,
   ZIGBEE_DEV_UNKNOW = 0xFF,

};

class ZigbeeData
{

protected:

       uint16_t u16PanId =0;
       uint16_t u16ShortAddr = 0xFFFF;
       uint16_t u16ParentAddr = 0xFFFF;
       uint64_t u64MacAddr = 0;
       uint8_t  u8DevType = 0xFF;
       uint8_t  u8Depth;
public:
       void setPanId(uint16_t u16Panid);
       uint16_t getPanId(void);

       void setParentAddr(uint16_t u16Addr);
       uint16_t getParentAddr(void);

       void setShortAddr(uint16_t u16Addr);
        uint16_t getShortAddr(void);

       void setMacAddr(uint64_t u64Addr);
       uint64_t getMacAddr(void);

       void setDevType(uint8_t u8Type);
       uint8_t  getDevType(void);

       void setDepth(uint8_t Depth);
       uint8_t  getDepth(void);
};


#endif // ZIGBEE_DATA_H
