#include "nwk_topology.h"


NwkTopology::NwkTopology(QWidget *parent):QLabel(parent)
{

}

NwkTopology::~NwkTopology()
{

}
void NwkTopology::set_pixmap(void)
{
    //this->pix = QPixmap(":/image/image/R.png");
    this->labelHeight = 60;// pix.rect().height();
    this->labelWidth = 60;// pix.rect().width();
}
void NwkTopology::show_pixmap(void)
{

    this->setGeometry( this->geometry().x(),  this->geometry().y(), this->labelWidth, this->labelHeight);
    //this->setPixmap(this->pix);
    this->show();
    this->setFrameShape(Box);
    this->bShow = true;
    //qDebug("pix.rect().width() %d,  pix.rect().height() %d", pix.rect().width(), pix.rect().height());
}
void NwkTopology::newNwkAddrLabel(void)
{
    this->nwkAddrLabel = new NwkTopology(this);
}
void NwkTopology::deleteNwkAddrLabel(void)
{
    delete nwkAddrLabel;
}
void NwkTopology::deleteDevTypeLabel(void)
{
     delete devTypeLabel;
}
void NwkTopology::newDevTypeLabel(void)
{
    this->devTypeLabel = new NwkTopology(this);
}

int NwkTopology::getLabelHeight(void)
{
   return this->labelHeight;
}

int NwkTopology::getLabelWidth(void)
{
   return this->labelWidth;
}

void NwkTopology::reset_bShow(void)
{
    this->bShow = false;
}

bool NwkTopology::get_bShow(void)
{
   return this->bShow;
}
void NwkTopology::setData(void)
{
   QFont font ("Microsoft YaHei", 10, 75);
   QPalette pa;
   pa.setColor(QPalette::WindowText,Qt::black);


   if(this->getDevType() == ZIGBEE_DEV_COOR)
   {
       this->setStyleSheet("background-color:red"); // 设置QLabel背景的颜色
       this->devTypeLabel->setText("C");
   }
   else if(this->getDevType() == ZIGBEE_DEV_ROUTER)
  {
       this->setStyleSheet("background-color:yellow"); // 设置QLabel背景的颜色
       this->devTypeLabel->setText("R");
   }
   else if(this->getDevType() == ZIGBEE_DEV_ENDDEVICE)
   {
        this->setStyleSheet("background-color:green"); // 设置QLabel背景的颜色
     this->devTypeLabel->setText("E");
   }
   else
       this->devTypeLabel->setText("X");//ZIGBEE_DEV_UNKNOW

   this->devTypeLabel->setGeometry(1, 10, this->labelWidth-2, 20);
   this->devTypeLabel->setFont(font);
   this->devTypeLabel->setPalette(pa);
   //this->devTypeLabel->setFrameShape(WinPanel);
   this->devTypeLabel->setAlignment(Qt::AlignCenter);
   //this->devTypeLabel->adjustSize();
   //this->devTypeLabel->raise();

   qDebug("addr %x", this->getShortAddr());
   this->nwkAddrLabel->setText(QString::asprintf("0x%04X",this->getShortAddr()));
   this->nwkAddrLabel->setGeometry(1, this->labelHeight-25, this->labelWidth-2, 20);
   this->nwkAddrLabel->setFont(font);
   this->nwkAddrLabel->setPalette(pa);
   //this->nwkAddrLabel->setFrameShape(WinPanel);
   this->nwkAddrLabel->setAlignment(Qt::AlignCenter);
   //this->nwkAddrLabel->adjustSize();
   //this->nwkAddrLabel->raise();
}


