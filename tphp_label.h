#ifndef TPHP_LABEL_H
#define TPHP_LABEL_H

#include <QLabel>
#include "zigbee_data.h"
#include "tphp.h"

class TPHP_Label : public QLabel, public ZigbeeData, public TPHP_Data
{
        Q_OBJECT

public:
       TPHP_Label(QWidget *parent=0);
       ~TPHP_Label();


private:

       QLabel *tempLabel;
       QLabel *humiLabel;
       QLabel *addrLabel;

       QPixmap pix;
       int labelHeight =0 ;
       int labelWidth = 0 ;

private slots:


public:


   void set_pixmap(void);
   void show_pixmap(void);
   void setData(void);
   void NodeLabeUpdate();

   void newTempLabel(void);
   void newHumiLabel(void);
   void newAddrLabel(void);

   int getLabelHeight(void);
   int getLabelWidth(void);
};


#endif // NODE_LABEL_H
