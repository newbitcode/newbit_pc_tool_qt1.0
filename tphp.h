#ifndef TPHP_H
#define TPHP_H


#include <QWidget>

class TPHP_Data
{

protected:
       int8_t  i8Temp = 0;
       uint8_t u8Humi = 0;

public:
       void setTemp(int8_t i8Temp);
       void setHumi(uint8_t u8Humi);

       int8_t getTemp(void);
       uint8_t getHumi(void);

};
#endif // TPHP_H
