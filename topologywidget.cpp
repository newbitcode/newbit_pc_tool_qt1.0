#include "topologywidget.h"
#include <QPainter>
#include <QWidget>

TopologyWidget::TopologyWidget(QWidget *parent) : QWidget(parent)
{

}
TopologyWidget::~TopologyWidget()
{

}

void TopologyWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    qDebug("paintEvent");

    drawArrow();
}
void TopologyWidget::drawArrow()
{
    linePainter = new QPainter(this);
    double LineX1, LineX2, LineY1, LineY2;
    linePainter->setRenderHint(QPainter::Antialiasing, true);
    linePainter->setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin));

    QPointF startPoint;
    QPointF endPoint;

    foreach (QLine *line, lineList) {

        startPoint.setX(line->x1());
        startPoint.setY(line->y1());
        endPoint.setX(line->x2());
        endPoint.setY(line->y2());
        //求得箭头两点坐标
        calcVertexes(startPoint.x(), startPoint.y(), endPoint.x(), endPoint.y(), LineX1, LineY1, LineX2, LineY2);
        linePainter->drawLine(startPoint, endPoint);//绘制线段
        linePainter->drawLine(endPoint.x(), endPoint.y(), LineX1, LineY1);//绘制箭头一半
        linePainter->drawLine(endPoint.x(), endPoint.y(), LineX2, LineY2);//绘制箭头另一半

    }

}
//求箭头两点坐标
void TopologyWidget::calcVertexes(double start_x, double start_y, double end_x, double end_y, double& x1, double& y1, double& x2, double& y2)
{
    double arrow_lenght_ = 10;//箭头长度，一般固定
    double arrow_degrees_ = 0.5;//箭头角度，一般固定

    double angle = atan2(end_y - start_y, end_x - start_x) + 3.1415926;//

    x1 = end_x + arrow_lenght_ * cos(angle - arrow_degrees_);//求得箭头点1坐标
    y1 = end_y + arrow_lenght_ * sin(angle - arrow_degrees_);
    x2 = end_x + arrow_lenght_ * cos(angle + arrow_degrees_);//求得箭头点2坐标
    y2 = end_y + arrow_lenght_ * sin(angle + arrow_degrees_);
}

void TopologyWidget::addArrows(double x1, double y1, double x2, double y2) //箭头的两点坐标
{
    QLine *line = new QLine;
    line->setLine(x1, y1, x2, y2);
    lineList.append(line);
}
void TopologyWidget::deleteAllArrows()
{

   lineList.clear();
}
