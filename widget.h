#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMovie>
#include <QLabel>
#include <QEvent>
#include <QPainter>
#include <QResizeEvent>
#include <QPixmap>
#include "tphp_label.h"
#include <QScrollArea>
#include <QMessageBox>
#include <QTime>
#include <QMouseEvent>
#include <QAbstractButton>
#include <QAbstractNativeEventFilter>
#include <windows.h>
#include <windowsx.h>
#include <nwk_topology.h>
#include "topologywidget.h"
#include <dbt.h>
#include <QScrollBar>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    virtual void timerEvent( QTimerEvent *event);

private:
    Ui::Widget *ui;
    QSerialPort *serial;
    QMovie *movie;

    QList<TPHP_Label *> tphpList;
    QList<NwkTopology *> topologyList;


    TopologyWidget *topologyWidget;


    QPushButton *clrButton;

    QLabel *textLabel;

    uint64_t TxBytes =0;
    uint64_t RxBytes =0;

    int w =0;
    int h =0;
    int autoRetryTimerId = 0;

    QScrollBar* m_vscrollBar;
signals:


private slots:
    void on_clearButton_clicked();
    void on_sendButton_clicked();
    void on_openButton_clicked();
    void Read_Data();
    void on_tabWidget_tabBarClicked(int index);
    //void on_clearNodeButton_clicked();
    void handleSerialError(QSerialPort::SerialPortError error);

    void on_autoRetry_clicked(bool checked);
    void clrButtonClicked(bool checked);

    void on_sendHistory_activated(int index);
    void on_autoRetryTimesBox_valueChanged(int arg1);
protected:
    void changeEvent(QEvent * event);
    void resizeEvent(QResizeEvent* size);
    void mouseReleaseEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void parseData(QByteArray byteArray);
    void nwkTopologyData(QByteArray byteArray);
    void tphpData(QByteArray byteArray);

    void topologyListRefresh(void);
    void tphpListRefresh(void);

    void findAvailablePorts(void);
    char convertHexChar(char ch);
    void stringToHex(QString str, QByteArray &arry);

    bool nativeEvent(const QByteArray &eventType, void *message, long *result);

   bool checkSum(QByteArray array);
   void setClrButtonLocation();
   //bool winEvent(MSG *msg,long *result);
    //void paintEvent(QPaintEvent *event);


};

static const GUID GUID_DEVINTERFACE_LIST[] =
    {
//    // GUID_DEVINTERFACE_USB_DEVICE
//    { 0xA5DCBF10, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } },
//    // GUID_DEVINTERFACE_DISK
//    { 0x53f56307, 0xb6bf, 0x11d0, { 0x94, 0xf2, 0x00, 0xa0, 0xc9, 0x1e, 0xfb, 0x8b } },
//    // GUID_DEVINTERFACE_HID,
//    { 0x4D1E55B2, 0xF16F, 0x11CF, { 0x88, 0xCB, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30 } },
//    // GUID_NDIS_LAN_CLASS
//    { 0xad498944, 0x762f, 0x11d0, { 0x8d, 0xcb, 0x00, 0xc0, 0x4f, 0xc3, 0x35, 0x8c } },
    // GUID_DEVINTERFACE_COMPORT
    //{ 0x86e0d1e0, 0x8089, 0x11d0, { 0x9c, 0xe4, 0x08, 0x00, 0x3e, 0x30, 0x1f, 0x73 } },
    // GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR
    { 0x4D36E978, 0xE325, 0x11CE, { 0xBF, 0xC1, 0x08, 0x00, 0x2B, 0xE1, 0x03, 0x18 } }
//    // GUID_DEVINTERFACE_PARALLEL
//    { 0x97F76EF0, 0xF883, 0x11D0, { 0xAF, 0x1F, 0x00, 0x00, 0xF8, 0x00, 0x84, 0x5C } },
//    // GUID_DEVINTERFACE_PARCLASS
//    { 0x811FC6A5, 0xF728, 0x11D0, { 0xA5, 0x37, 0x00, 0x00, 0xF8, 0x75, 0x3E, 0xD1 } }
    };
#endif // WIDGET_H
