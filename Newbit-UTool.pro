#-------------------------------------------------
#
# Project created by QtCreator 2018-05-14T17:39:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += serialport

TARGET = Newbit-UTool
TEMPLATE = app

#DEFINES += DEFINE_GUID


SOURCES += main.cpp\
        widget.cpp \
    zigbee_data.cpp \
    tphp.cpp \
    tphp_label.cpp \
    nwk_topology.cpp \
    topologywidget.cpp

HEADERS  += widget.h \
    zigbee_data.h \
    tphp.h \
    tphp_label.h \
    nwk_topology.h \
    topologywidget.h

FORMS    += widget.ui

RESOURCES += \
    image.qrc

RC_FILE += \
    Newbit-UTool.rc

