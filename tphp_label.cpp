#include "tphp_label.h"
#include <QDebug>

TPHP_Label::TPHP_Label(QWidget *parent):QLabel(parent)
{

}

TPHP_Label::~TPHP_Label()
{

}


void TPHP_Label::set_pixmap(void)
{
    this->pix = QPixmap(":/image/image/ht5x5.png");
    this->labelHeight =  pix.rect().height();
    this->labelWidth =  pix.rect().width();
}

void TPHP_Label::show_pixmap(void)
{

    this->setGeometry( this->geometry().x(),  this->geometry().y(), pix.rect().width(), pix.rect().height());
    this->setPixmap(this->pix);
    this->show();
    this->setFrameShape(Box);

    //qDebug("pix.rect().width() %d,  pix.rect().height() %d", pix.rect().width(), pix.rect().height());
}
void TPHP_Label::setData(void)
{
   QFont font ("Microsoft YaHei", 10, 75);
   QPalette pa;
   pa.setColor(QPalette::WindowText,Qt::white);

   //qDebug()<<"getHumi: " << this->getHumi();
   this->humiLabel->setText("  " + QString::number(this->getHumi(), 10) + "\r\n" + "%RH");
   this->humiLabel->setGeometry(7, 35, 20, 20);
   this->humiLabel->setFont(font);
   this->humiLabel->setPalette(pa);
   //this->humiLabel->setFrameShape(WinPanel);
   this->tempLabel->setAlignment(Qt::AlignCenter);
   this->humiLabel->adjustSize();
   this->humiLabel->raise();


   //qDebug()<<"getTemp: " << this->getTemp();
   this->tempLabel->setText(QString::number(this->getTemp(), 10) + "\r\n" + "°C");
   this->tempLabel->setGeometry(105, 35, 20, 20);
   this->tempLabel->setFont(font);
   this->tempLabel->setPalette(pa);
   //this->tempLabel->setFrameShape(WinPanel);
   this->tempLabel->setAlignment(Qt::AlignCenter);
   this->tempLabel->adjustSize();
   this->tempLabel->raise();



   //qDebug()<<"Addr: " <<QString::number(this->getShortAddr(), 16).toUpper();
   this->addrLabel->setText(QString::number(this->getShortAddr(), 16).toUpper());
   this->addrLabel->setGeometry(1, 141, this->labelWidth -2, 20);
   this->addrLabel->setFont(font);
   this->addrLabel->setPalette(pa);
   this->addrLabel->setAlignment(Qt::AlignCenter);
   //this->addrLabel->setFrameShape(WinPanel);
   //this->addrLabel->adjustSize();
   this->addrLabel->raise();

}

void TPHP_Label::NodeLabeUpdate(void)
{
   this->setData();
   this->show_pixmap();
}

void TPHP_Label::newTempLabel(void)
{
    this->tempLabel = new TPHP_Label(this);
}
void TPHP_Label::newHumiLabel(void)
{
    this->humiLabel = new TPHP_Label(this);
}
void TPHP_Label::newAddrLabel(void)
{
   this->addrLabel = new TPHP_Label(this);
}
int TPHP_Label::getLabelHeight(void)
{
   return this->labelHeight;
}

int TPHP_Label::getLabelWidth(void)
{
   return this->labelWidth;
}
