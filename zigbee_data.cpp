#include "zigbee_data.h"



void ZigbeeData::setPanId(uint16_t u16Panid)
{
   this->u16PanId = u16Panid;
}
uint16_t ZigbeeData::getPanId(void)
{
  return this->u16PanId;
}


void ZigbeeData::setParentAddr(uint16_t u16Addr)
{
   this->u16ParentAddr = u16Addr;
}
uint16_t ZigbeeData::getParentAddr(void)
{
  return this->u16ParentAddr;
}

void ZigbeeData::setShortAddr(uint16_t u16Addr)
{
   this->u16ShortAddr = u16Addr;
}
uint16_t ZigbeeData::getShortAddr(void)
{
  return this->u16ShortAddr;
}


void ZigbeeData::setMacAddr(uint64_t u64Addr)
{
   this->u64MacAddr = u64Addr;
}
uint64_t ZigbeeData::getMacAddr(void)
{
  return this->u64MacAddr;
}


void ZigbeeData::setDevType(uint8_t u8Type)
{
   this->u8DevType = u8Type;
}
uint8_t ZigbeeData::getDevType(void)
{
  return this->u8DevType;
}

void ZigbeeData::setDepth(uint8_t Depth)
{
   this->u8Depth = Depth;
}
uint8_t ZigbeeData::getDepth(void)
{
  return this->u8Depth;
}

