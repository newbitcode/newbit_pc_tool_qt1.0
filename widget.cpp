#include "widget.h"
#include "ui_widget.h"
#include <QTimer>
/**************************************************************************************************
 * @fn      Widget
 *
 * @brief   构造函数
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

//    setWindowFlags(windowFlags()&~Qt::WindowMaximizeButtonHint);    // 禁止最大化按钮
//    setFixedSize(this->width(),this->height());                     // 禁止拖动窗口大小


     topologyWidget = new TopologyWidget(ui->tabWidget);  //显示拓扑图的Tab界面的子界面

     ui->tabWidget->insertTab(2, topologyWidget, "拓扑");
     topologyWidget->adjustSize();

     clrButton = new QPushButton(topologyWidget);

     clrButton->setText("清空");
     clrButton->resize(75, 23);
     QObject::connect(clrButton, &QPushButton::clicked, this, &Widget::clrButtonClicked);

    //查找可用的串口
       findAvailablePorts();
       //设置波特率下拉菜单默认显示第三项
       ui->BaudBox->setCurrentIndex(6);
       ui->BitNumBox->setCurrentIndex(3);
       //关闭发送按钮的使能
       ui->sendButton->setEnabled(false);
       qDebug() << tr("界面设定成功！");

       this->setWindowTitle("Newbit-UTool                                           www.newbitstudio.com");
       //this->setStyleSheet("background-color:rgb(199,237,204)");
       ui->tabWidget->setTabText(0, "串口");
       ui->tabWidget->setTabText(1, "TPHP");
       ui->tabWidget->setTabText(2, "拓扑");

       QFont font("Arial", 10);
       ui->receiveTextBrowser->setFont(font);
       ui->receiveTextBrowser->clear();

       ui->tab_2->setToolTip("温湿度显示区域");
       topologyWidget->setToolTip("网络拓扑显示区域");
       textLabel = new QLabel(ui->tab_2);
       TxBytes = 0;
       RxBytes = 0;
       ui->txRxBytes->setText(QString::asprintf("Tx:%llu Byte    Rx:%llu Byte", TxBytes, RxBytes));
       ui->txRxBytes->adjustSize();
       ui->comLabel->setText(" ");
       ui->comLabel->adjustSize();
}
/**************************************************************************************************
 * @fn      Widget
 *
 * @brief   析构函数
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
Widget::~Widget()
{
    delete ui;
}

/**************************************************************************************************
 * @fn      on_clearButton_clicked
 *
 * @brief   清空接收串口
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_clearButton_clicked()
{
    ui->receiveTextBrowser->clear();
    qDebug() << tr("textEdit->clear();");
    TxBytes = 0;
    RxBytes = 0;
    ui->txRxBytes->setText(QString::asprintf("Tx:%llu Bytes    Rx:%llu Bytes", TxBytes, RxBytes));
}
/**************************************************************************************************
 * @fn      on_sendButton_clicked
 *
 * @brief   发送数据到串口
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_sendButton_clicked()
{
    if(ui->sendTextEdit->toPlainText().size()&& serial->isOpen())
    {
        if(ui->sendASCII->isChecked())
        {
           serial->write(ui->sendTextEdit->toPlainText().toLatin1());
           TxBytes += ui->sendTextEdit->toPlainText().toLatin1().size();

           ui->txRxBytes->setText(QString::asprintf("Tx:%llu Bytes    Rx:%llu Bytes", TxBytes, RxBytes));
        }
        else
        {
            QString str = ui->sendTextEdit->toPlainText();
            QByteArray arry;

            stringToHex(str, arry);

            serial->write(arry);
            TxBytes += arry.size();
            ui->txRxBytes->setText(QString::asprintf("Tx:%llu Bytes    Rx:%llu Bytes", TxBytes, RxBytes));
        }
        if(ui->displaySend->isChecked())
        {
            QString str;

            if(ui->displayTimes->isChecked())
            {
                QTime time = QTime::currentTime();
                str.append("\r\n");
                str.append('[' + time.toString()+ ']'+ ' ');
            }
            else if(ui->autoLineFeed->isChecked() && str.size())
                str.append("\r\n");

            str.append(ui->sendTextEdit->toPlainText());
            ui->receiveTextBrowser->append(str);

        }
        if(ui->sendHistory->findText(ui->sendTextEdit->toPlainText())<0)
        {
            ui->sendHistory->insertItem(0,ui->sendTextEdit->toPlainText());
            ui->sendHistory->setCurrentIndex(0);
        }
        else
        {
            ui->sendHistory->removeItem(ui->sendHistory->findText(ui->sendTextEdit->toPlainText()));
            ui->sendHistory->insertItem(0,ui->sendTextEdit->toPlainText());
            ui->sendHistory->setCurrentIndex(0);
        }
    }
    else
    {

    }
}

/**************************************************************************************************
 * @fn      Read_Data
 *
 * @brief   从串口读取接收到的数据
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::Read_Data()
{
    QByteArray buf;
    buf = serial->readAll();

    //qDebug() << buf.toHex();
    //qDebug() << buf;
    if(!buf.isEmpty())
    {
        QString str;
        RxBytes += buf.size();


        if(ui->displayTimes->isChecked())
        {
            QTime time = QTime::currentTime();
            str.append("\r\n");
            str.append('[' + time.toString()+ ']'+ ' ');
        }
        else if(ui->autoLineFeed->isChecked() && str.size())
            str.append("\r\n");

        if(ui->receiveHex->isChecked())
        {

            QString strHex = buf.toHex().toUpper();

            for(int i =2; i < strHex.size(); i = i+3)
                strHex.insert(i, ' ');


            ui->receiveTextBrowser->append(str + strHex);

        }
        else
        {
           str += tr(buf);
           ui->receiveTextBrowser->append(str);
        }
    }

  if(checkSum(buf))
     parseData(buf);

    buf.clear();

    //qDebug() << tr("new data");
     ui->txRxBytes->setText(QString::asprintf("Tx:%llu Bytes    Rx:%llu Bytes", TxBytes, RxBytes));

}
/**************************************************************************************************
 * @fn      on_openButton_clicked
 *
 * @brief   配置串口
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_openButton_clicked()
{
    if(ui->openButton->text()==tr("打开串口"))
    {
        serial = new QSerialPort;
        //设置串口名
        QString portName = ui->PortBox->currentText();

        portName = portName.mid(portName.lastIndexOf("COM"));
        portName = portName.remove(')');
        qDebug() << portName;
        serial->setPortName(portName);
        ui->comLabel->setText(portName + "    ");
        ui->comLabel->adjustSize();
        //打开串口
        if(serial->open(QIODevice::ReadWrite))
        {
            //设置波特率
            serial->setBaudRate(ui->BaudBox->currentText().toInt());
            //设置数据位数
            switch(ui->BitNumBox->currentIndex())
            {
            case 0: serial->setDataBits(QSerialPort::Data5); break;
            case 1: serial->setDataBits(QSerialPort::Data6); break;
            case 2: serial->setDataBits(QSerialPort::Data7); break;
            case 3: serial->setDataBits(QSerialPort::Data8); break;
            default: break;
            }
            //设置奇偶校验
            switch(ui->ParityBox->currentIndex())
            {

            case 0: serial->setParity(QSerialPort::NoParity); break;
            case 1: serial->setParity(QSerialPort::EvenParity); break;
            case 2: serial->setParity(QSerialPort::OddParity); break;
            default: break;
            }
            //设置停止位
            switch(ui->StopBox->currentIndex())
            {
            case 0: serial->setStopBits(QSerialPort::OneStop); break;
            case 1: serial->setStopBits(QSerialPort::OneAndHalfStop); break;
            case 2: serial->setStopBits(QSerialPort::TwoStop); break;
            default: break;
            }
            //设置流控制
            serial->setFlowControl(QSerialPort::NoFlowControl);
            //关闭设置菜单使能
            ui->PortBox->setEnabled(false);
            ui->BaudBox->setEnabled(false);
            ui->BitNumBox->setEnabled(false);
            ui->ParityBox->setEnabled(false);
            ui->StopBox->setEnabled(false);
            ui->openButton->setText(tr("关闭串口"));
            ui->sendButton->setEnabled(true);
            //连接信号槽
            QObject::connect(serial, &QSerialPort::readyRead, this, &Widget::Read_Data);
            QObject::connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleSerialError(QSerialPort::SerialPortError)));
        }
        else
        {
           //QSerialPort::SerialPortError err = serial->error();
           QMessageBox::critical(this, tr("Error"), "串口已断开，请检查是否正确连接！");
           //已增加热插拔
           //findAvailablePorts();
        }
    }
    else
    {
        //关闭串口
        serial->clear();
        serial->close();
        serial->deleteLater();
        //恢复设置使能
        ui->PortBox->setEnabled(true);
        ui->BaudBox->setEnabled(true);
        ui->BitNumBox->setEnabled(true);
        ui->ParityBox->setEnabled(true);
        ui->StopBox->setEnabled(true);
        ui->openButton->setText(tr("打开串口"));
        ui->sendButton->setEnabled(false);

        on_autoRetry_clicked(false);
        //已增加热插拔
        //findAvailablePorts();
    }
}
/**************************************************************************************************
 * @fn      on_tabWidget_tabBarClicked
 *
 * @brief   根据选项卡index执行...
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_tabWidget_tabBarClicked(int index)
{
    qDebug("Widget_tabBarClicked index %d", index);
    ui->tabWidget->setCurrentIndex(index);
    //qDebug("index %d", ui->tabWidget->indexOf(ui->tabWidget->currentWidget()));
    switch (index)
    {
        case 0:

        break;

        case 1:
                tphpListRefresh();

        break;

        case 2:

                topologyListRefresh();

        break;

        default:
            break;

    }
  setClrButtonLocation();
}

/**************************************************************************************************
 * @fn      changeEvent
 *
 * @brief   窗口变化
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::changeEvent(QEvent * event)
{

   if(event->type()==QEvent::WindowStateChange)
   {
       if(ui->tabWidget->currentIndex() == 1)
       {
           //tphpListRefresh();
           qDebug("WindowStateChange");
       }
   }

}
/**************************************************************************************************
 * @fn      mouseReleaseEvent
 *
 * @brief   鼠标释放
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::mouseReleaseEvent(QMouseEvent *e)
{

//    if(ui->tabWidget->currentIndex() == 1 && e->button() == Qt::LeftButton)
//    {
//       //qDebug("mouseReleaseEvent");

//    }

}
/**************************************************************************************************
 * @fn      mousePressEvent
 *
 * @brief   鼠标按下
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::mousePressEvent(QMouseEvent *e)
{

//    if(ui->tabWidget->currentIndex() == 1 && e->button() == Qt::LeftButton)
//    {
//       //qDebug("mousePressEvent");

//    }
}
/**************************************************************************************************
 * @fn      mouseMoveEvent
 *
 * @brief   鼠标移动
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::mouseMoveEvent(QMouseEvent *e)
{
//    (void)e;
//    if(ui->tabWidget->currentIndex() > 0)
//    {
//       //qDebug("mouseMoveEvent");
//       // tphpListRefresh();

//    }
}
/**************************************************************************************************
 * @fn      resizeEvent
 *
 * @brief   窗口缩放
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::resizeEvent(QResizeEvent* size)
{

    (void)size;
     qDebug("resizeEvent");
    //tphpListRefresh();

    setClrButtonLocation();
}
/**************************************************************************************************
 * @fn      parseData
 *
 * @brief   解析数据
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void  Widget::parseData(QByteArray byteArray)
{
   uint16_t cmd = (byteArray.at(NEWBIT_TOOL_CMD_INDEX) & 0xff)|((byteArray.at(NEWBIT_TOOL_CMD_INDEX+1) & 0xff) << 8);

   switch(cmd)
   {
      case NEWBIT_TOOL_CMD_TOPOLOGY:

         if(byteArray.size() == TOPOLOGY_CMD_DATA_LENGTH)
          nwkTopologyData( byteArray);
       break;

       case NEWBIT_TOOL_CMD_TPHP:
            if(byteArray.size() == TPHP_CMD_DATA_LENGTH)
            tphpData( byteArray);
       break;

   }
   setClrButtonLocation();
}

/**************************************************************************************************
 * @fn      nwkTopologyData
 *
 * @brief   网络拓扑数据
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::nwkTopologyData(QByteArray byteArray)
{
    char flag =1;

    uint64_t u64Addr;
    uint16_t addr;
    QByteArray Array = byteArray.mid(TOPOLOGY_MAC_ADDR_INDEX, TOPOLOGY_MAC_ADDR_LENGTH);
    memcpy(&u64Addr, Array, TOPOLOGY_MAC_ADDR_LENGTH);
    //uint16_t PianId = byteArray.at(TOPOLOGY_PAN_ID_INDEX)| (byteArray.at(TOPOLOGY_PAN_ID_INDEX+1) << 8);
    if(topologyList.size())
    {
        foreach(NwkTopology *List, topologyList)
        {
           if( List->getMacAddr() == u64Addr)
           {
               flag =0;

               Array = byteArray.mid(TOPOLOGY_PAN_ID_INDEX, TOPOLOGY_PAN_ID_LENGTH);
               memcpy(&addr, Array, TOPOLOGY_PAN_ID_LENGTH);
               List->setPanId(addr);

               Array = byteArray.mid(TOPOLOGY_PARENT_ADDR_INDEX, TOPOLOGY_PARENT_ADDR_LENGTH);
               memcpy(&addr, Array, TOPOLOGY_PARENT_ADDR_LENGTH);
               List->setParentAddr( addr);

               Array = byteArray.mid(TOPOLOGY_SHORT_ADDR_INDEX, TOPOLOGY_SHORT_ADDR_LENGTH);
               memcpy(&addr, Array, TOPOLOGY_SHORT_ADDR_LENGTH);
               List->setShortAddr(addr);

               List->setDevType(byteArray.at(TOPOLOGY_DEV_TYPE_INDEX));
               List->setDepth(byteArray.at(TOPOLOGY_DEPTH_INDEX));

//              qDebug()<< QString::asprintf("byteArray: 0x%04X",addr);
//              qDebug()<< QString::asprintf("u64Addr: 0x%016llX",u64Addr);
           }

        }
    }
    if(flag)
    {
        NwkTopology *List = new NwkTopology(topologyWidget);

        List->newDevTypeLabel();
        List->newNwkAddrLabel();

        List->setMacAddr(u64Addr);

        Array = byteArray.mid(TOPOLOGY_PAN_ID_INDEX, TOPOLOGY_PAN_ID_LENGTH);
        memcpy(&addr, Array, TOPOLOGY_PAN_ID_LENGTH);
        List->setPanId(addr);

        Array = byteArray.mid(TOPOLOGY_PARENT_ADDR_INDEX, TOPOLOGY_PARENT_ADDR_LENGTH);
        memcpy(&addr, Array, TOPOLOGY_PARENT_ADDR_LENGTH);
        List->setParentAddr( addr);

        Array = byteArray.mid(TOPOLOGY_SHORT_ADDR_INDEX, TOPOLOGY_SHORT_ADDR_LENGTH);
        memcpy(&addr, Array, TOPOLOGY_SHORT_ADDR_LENGTH);
        List->setShortAddr(addr);

        List->setDevType(byteArray.at(TOPOLOGY_DEV_TYPE_INDEX));
        List->setDepth(byteArray.at(TOPOLOGY_DEPTH_INDEX));

        List->set_pixmap();
        this->topologyList.append(List);


//        qDebug()<< QString::asprintf("byteArray: 0x%04X",byteArray.at(TOPOLOGY_SHORT_ADDR_INDEX) |((byteArray.at(TOPOLOGY_SHORT_ADDR_INDEX +1) << 8)));

    }
    topologyListRefresh();
}

/**************************************************************************************************
 * @fn      topologyListRefresh
 *
 * @brief   刷新显示
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void  Widget::topologyListRefresh(void)
{
    int size  = this->topologyList.size();
    int current_y = 0, current_x =0;
    int labelHeight =0, labelWidth =0;
    int currentRowNodeNumber =0;
    int reduceWidth,reduceHeight;
    uint8_t depthNode[35] = {0};
    uint8_t maxNodeDepth = 0;
    bool foundCoor = false;

    int grap = 25;
    int MaxDepth = 0;

    if(topologyList.size() == 0)
    {
        textLabel->setParent(topologyWidget);
        textLabel->setText("暂无网络拓扑数据");
        textLabel->setGeometry(topologyWidget->x(),topologyWidget->height() / 2, topologyWidget->width(), 20);
        textLabel->setAlignment(Qt::AlignCenter);
        textLabel->show();
        clrButton->hide();
        clrButton->setEnabled(false);
    }
    else
    {
        textLabel->hide();
        clrButton->show();
        clrButton->setEnabled(true);
    }
    if(size == 0)
    {
      return;
    }
    qDebug("topologyList.size(): %d ", size);
    if(ui->tabWidget->currentWidget() == topologyWidget)
    {
        //找出最大深度
        foreach(NwkTopology *List, this->topologyList)
        {

            MaxDepth = (MaxDepth > List->getDepth())? MaxDepth:List->getDepth();
            //qDebug("List->getDepth(): %d ", List->getDepth());
            if(List->getShortAddr() == 0 && List->getDevType() == ZIGBEE_DEV_COOR)
            {
              //qDebug("List %04x %04x", List->getShortAddr(), List->getParentAddr());
              List->move( 0, 0);
              List->setData();
              List->show_pixmap();
              foundCoor = true;
            }
        }
        if(!foundCoor)
            return;

        //保存每一个深度有多少个节点
        for(int i=0; i< MaxDepth+1; i++)
        {
             foreach(NwkTopology *List, this->topologyList)
             {
                 if(List->getDepth() == i)
                     depthNode[i]++;

             }
        }
        //哪一行最多节点？是多少个？
        for(int i =0; i< MaxDepth+1; i++)
        {
           maxNodeDepth = (maxNodeDepth > depthNode[i])?maxNodeDepth:depthNode[i];
        }

        labelHeight = this->topologyList.at(0)->getLabelHeight();
        labelWidth = this->topologyList.at(0)->getLabelWidth();
        grap =  labelHeight / 2;

        reduceWidth = topologyWidget->width() - (maxNodeDepth *labelWidth + (maxNodeDepth - 1) * grap);
        reduceHeight = topologyWidget->height() - ((MaxDepth + 1) * labelHeight + MaxDepth * grap);
        //适配窗口
        this->resize(this->width()-reduceWidth , this->height() - reduceHeight);

        topologyWidget->deleteAllArrows();
        for(int i =0; i< MaxDepth+1; i++)
        {
            current_x =0;
            currentRowNodeNumber = 0;
            foreach(NwkTopology *fatherList, this->topologyList)
            {
                if(fatherList->getDepth() == i && fatherList->getDevType() <ZIGBEE_DEV_ENDDEVICE)
                {
                    foreach(NwkTopology *sunList, this->topologyList)
                    {
                        if(sunList->getParentAddr() == fatherList->getShortAddr() && sunList->getParentAddr() != 0xFFFF && sunList->getDevType() > ZIGBEE_DEV_COOR)
                        {

                             current_y = fatherList->y() + fatherList->height() + grap;
                             current_x = currentRowNodeNumber *(grap + labelWidth) ;
                             currentRowNodeNumber++;
                             sunList->move(current_x, current_y);
                             sunList->setData();
                             sunList->show_pixmap();

                             topologyWidget->addArrows(sunList->x()+sunList->width()/2,
                                                       sunList->y(),
                                                       fatherList->x() + fatherList->width()/2,
                                                       fatherList->y() + fatherList->height() );

                        }


                    }

                }
            }
        }
//        qDebug("for over");

        topologyWidget->update();

    }
  // qDebug("this->width() %d, this->height() %d", this->width(), this->height());

}
/**************************************************************************************************
 * @fn      tphpData
 *
 * @brief   温湿度数据
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::tphpData(QByteArray byteArray)
{
    char flag =1;
    uint64_t u64Addr = 0;
    QByteArray Array = byteArray.mid(TPHP_MAC_ADDR_INDEX, TPHP_MAC_ADDR_LENGTH);

    memcpy(&u64Addr, Array, TPHP_MAC_ADDR_LENGTH);
    if(this->tphpList.size())
    {
        foreach(TPHP_Label *List, this->tphpList)
        {
           if(List->getMacAddr() == u64Addr)
           {
               flag =0;

               List->setTemp(byteArray.at(TPHP_TEMP_DATA_INDEX) & 0xff);
               List->setHumi(byteArray.at(TPHP_HUMI_DATA_INDEX) & 0xff);
               List->setShortAddr((byteArray.at(TPHP_SHORT_ADDR_INDEX) & 0xff)|((byteArray.at(TPHP_SHORT_ADDR_INDEX+1) & 0xff) << 8));
               qDebug()<< QString::asprintf("u64Addr: 0x%016llX",u64Addr);
              // qDebug("getMacAddr() == u64Addr");
               //qDebug("temp %d, humi %d", List->getTemp(), List->getHumi());
           }
        }
    }
    if(1 == flag)
    {

        TPHP_Label *Label = new TPHP_Label(ui->tab_2);

        Label->setTemp(byteArray.at(TPHP_TEMP_DATA_INDEX) & 0xff);
        Label->setHumi(byteArray.at(TPHP_HUMI_DATA_INDEX) & 0xff);

        Label->setShortAddr((byteArray.at(TPHP_SHORT_ADDR_INDEX) & 0xff)|((byteArray.at(TPHP_SHORT_ADDR_INDEX+1) & 0xff) << 8));
        Label->setMacAddr(u64Addr);
        Label->newTempLabel();
        Label->newHumiLabel();
        Label->newAddrLabel();
        Label->set_pixmap();
        this->tphpList.append(Label);

       // qDebug("nodeList.append(TPHP_Label)");
    }

    tphpListRefresh();
    //qDebug(byteArray.toHex());
    //qDebug() << QString::number(u64Addr, 16).toUpper();
}
/**************************************************************************************************
 * @fn      tphpListRefresh
 *
 * @brief   刷新显示温湿度数据
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void  Widget::tphpListRefresh(void)
{
    int x = 0 ,y = 0, c = 0, r = 0;
    int size  = this->tphpList.size();
    int node_r =0, node_c =0, real_r = 0, real_c = 0;
    int reduceHeight =0, reduceWidth =0;


    if(tphpList.size() == 0)
    {
        textLabel->setParent(ui->tab_2);
        textLabel->setText("暂无温湿度数据");
        textLabel->setGeometry(ui->tab_2->x(),ui->tab_2->height() / 2, ui->tab_2->width(), 20);
        textLabel->setAlignment(Qt::AlignCenter);
        textLabel->show();
        clrButton->setEnabled(false);
    }else
    {
        textLabel->hide();
        clrButton->show();
        clrButton->setEnabled(true);
    }



    if(1 > size)
    return;

    //qDebug("size: %d index %d", size, ui->tabWidget->currentIndex());

    if(ui->tabWidget->currentIndex() == 1)
    {
        c = ui->tab_2->width() / ( this->tphpList.at(0)->getLabelWidth() );
        r = ui->tab_2->height() / ( this->tphpList.at(0)->getLabelHeight() );

        node_r = (size % c)?(size / c +1):(size / c );
        node_c = (size > c)?c:size;

        r = ( r < node_r)?node_r:r;

        reduceHeight = ui->tab_2->height() - node_r* this->tphpList.at(0)->getLabelHeight();
        reduceWidth = ui->tab_2->width() - node_c* this->tphpList.at(0)->getLabelWidth();
        qDebug("c %d, r %d, node_c %d,  node_r %d", c, r, node_c, node_r);

        if(this->tphpList.size())
        foreach(TPHP_Label *List, this->tphpList)
        {

             if(real_c == c)
             {
                 x = 0;
                 real_r++;
                 real_c = 0;
             }

             y = real_r * List->getLabelHeight();
             x = real_c * List->getLabelWidth();
             real_c++;

             List->move(x, y);
             List->NodeLabeUpdate();

        }

        this->resize(this->width() - reduceWidth , this->height() - reduceHeight);

    }
  // qDebug("this->width() %d, this->height() %d", this->width(), this->height());

}

/**************************************************************************************************
 * @fn      handleSerialError
 *
 * @brief   处理串口错误事件
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::handleSerialError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Error"), "串口连接中断，请检查是否正确连接！");
        // QMessageBox::critical(this, tr("Error"), com->errorString());

    }
    QMessageBox::critical(this, tr("Error"), serial->errorString());
    findAvailablePorts();
}

void Widget::findAvailablePorts(void)
{
    ui->PortBox->clear();
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QSerialPort serial;
        serial.setPort(info);

        //if(serial.open(QIODevice::ReadWrite))
        {
            QString str = info.description();
            //QString str = info.manufacturer();
            qDebug() <<str;
            str.append('(');
            str.append(serial.portName());
            str.append( ')' );
            ui->PortBox->addItem(str);
            serial.close();
        }

    }
}


/**************************************************************************************************
 * @fn      convertHexChar
 *
 * @brief   转换成16进制字符
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
char Widget::convertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}

/**************************************************************************************************
 * @fn      stringToHex
 *
 * @brief   转换成16进制字符
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::stringToHex(QString str, QByteArray &arry)
{
    QStringList strList = str.split(' ');
    char c1,c2;

    str.clear();
    foreach(QString strl, strList)
    {
        str.append(strl);
    }

    for(int i =0; i < str.size(); i += 2)
    {
        c1 = convertHexChar(str[i].toLatin1());
        c2 = convertHexChar(str[i+1].toLatin1());
        if((c1 == -1) || (c2 == -1))
        {
             QMessageBox::critical(this, tr("Error"), "请输入16进制数");
        }
        else
        {
            arry.append(c1*16 + c2);
        }

    }
}
/**************************************************************************************************
 * @fn      timerEvent
 *
 * @brief   定时器溢出处理
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::timerEvent( QTimerEvent *event)
{

   if(event->timerId() == this->autoRetryTimerId && serial)
   {
       on_sendButton_clicked();
       //qDebug("timerEvent");
   }

}
/**************************************************************************************************
 * @fn      on_autoRetry_clicked
 *
 * @brief   自动重发
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_autoRetry_clicked(bool checked)
{
    static char flag =0;
    if(checked && ui->sendTextEdit->toPlainText().size() && serial->isOpen())
    {
        flag = 1;
        this->autoRetryTimerId = this->startTimer(ui->autoRetryTimesBox->text().toInt());
        qDebug("start autoRetry Timer");
    }
    else if(1 == flag){

        flag =0;
        this->killTimer(this->autoRetryTimerId);
        qDebug("kill autoRetry Timer");
    }

}
/**************************************************************************************************
 * @fn      nativeEvent
 *
 * @brief   鼠标事件
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
bool Widget::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    (void)(result);

    if (eventType == "windows_generic_MSG" || eventType == "windows_dispatcher_MSG")
    {
        static bool bResize = false;
        MSG * pMsg = reinterpret_cast<MSG *>(message);
        PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)pMsg->lParam;
        // qDebug("pMsg->message %X", pMsg->message);
        switch (pMsg->message)
        {

            case WM_NCMOUSEMOVE:
            case WM_MOUSEMOVE:
            {

                if(bResize)
                {
                    bResize =false;
                    qDebug("WM_NCMOUSEMOVE MOUSEMOVE:");

                    if(this->w != this->width() || this->h != this->height())
                    {
                        tphpListRefresh();
                        topologyListRefresh();
                    }

                    this->w = this->width();
                    this->h = this->height();
                }

                 //qDebug("x %d, y %d, w %d, h %d", this->x(), this->y(),  this->width(), this->height());
                 break;
            }

        case WM_NCLBUTTONDOWN:
        {

              bResize = true;
              qDebug("WM_NCLBUTTONDOWN");

             //qDebug("x %d, y %d, w %d, h %d", this->x(), this->y(),  this->width(), this->height());
             break;
        }
        case WM_DEVICECHANGE:
            switch (pMsg->wParam) {
               case DBT_DEVICEARRIVAL:

                    if( DBT_DEVTYP_PORT == lpdb->dbch_devicetype )
                    {
                        qDebug("DBT_插入 %ld", lpdb->dbch_devicetype);
                        findAvailablePorts();


                    }


                break;

                case DBT_DEVICEREMOVECOMPLETE:

                    if( DBT_DEVTYP_PORT == lpdb->dbch_devicetype )
                    {
                        qDebug("DBT_移除 %ld", lpdb->dbch_devicetype);
                        findAvailablePorts();

                    }


                break;
            }
            break;
        }

        //qDebug()<< QTime::currentTime() << pMsg->message;
    }
   setClrButtonLocation();
   return false;
}

/**************************************************************************************************
 * @fn      clrButtonClicked
 *
 * @brief   按下清空按钮
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::clrButtonClicked(bool checked)
{

   (void)checked;
   if(ui->tabWidget->currentIndex() == 2)
   {
       foreach(NwkTopology *sunList, this->topologyList)
       {
         sunList->deleteLater();
       }
       topologyList.clear();
       topologyWidget->deleteAllArrows();
       topologyWidget->update();
       qDebug("clrTopo_clicked");
       topologyListRefresh();
   }

   if(ui->tabWidget->currentIndex() == 1)
   {
       for(int i =0; i < this->tphpList.size(); i++)
       {
           TPHP_Label *label = this->tphpList.takeAt(i);

           delete label;
       }
       if(this->tphpList.size())
       {
           clrButtonClicked(0);
       }
       tphpListRefresh();
       qDebug("clearTphp_clicked");
   }

}

/**************************************************************************************************
 * @fn      checkSum
 *
 * @brief   校验和
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
bool Widget::checkSum(QByteArray array)
{

   uint8_t check = 0;

   for(int i =0; i < array.size()-1; i++)
   {
       check += (uint8_t)array.at(i);

   }
   if(check == ((uint8_t)array.at(array.size()-1)))
       return true;

   else
   {
       qDebug("Check err...check %d, array.last %d", check, array.at(array.size()-1));
       return false;

   }
}
/**************************************************************************************************
 * @fn      setClrButtonLocation
 *
 * @brief   设置清空按钮位置
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::setClrButtonLocation()
{
    if(ui->tabWidget->currentIndex() > 0)
    {
        clrButton->setParent(ui->tabWidget->currentWidget());
        ui->tabWidget->currentWidget()->show();
        clrButton->show();
        clrButton->move(ui->tabWidget->currentWidget()->x()+ ui->tabWidget->currentWidget()->width() - clrButton->width(),
                        ui->tabWidget->currentWidget()->y()+ ui->tabWidget->currentWidget()->height() - clrButton->height());
    }

}

/**************************************************************************************************
 * @fn      on_sendHistory_activated
 *
 * @brief   保存发送历史
 *
 * @param
 *
 * @return  None
 **************************************************************************************************/
void Widget::on_sendHistory_activated(int index)
{
    (void)index;
    ui->sendTextEdit->clear();
    ui->sendTextEdit->append(ui->sendHistory->currentText());
}


void Widget::on_autoRetryTimesBox_valueChanged(int arg1)
{
    on_autoRetry_clicked(true);
}
