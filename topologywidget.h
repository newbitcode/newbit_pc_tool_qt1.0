#ifndef TOPOLOGYWIDGET_H
#define TOPOLOGYWIDGET_H

#include <QWidget>
#include <QPainter>
class TopologyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TopologyWidget(QWidget *parent = 0);
    ~TopologyWidget();

    void paintEvent(QPaintEvent *event);


    void addArrows(double x1, double y1, double x2, double y2); //箭头的两点坐标
    void deleteAllArrows(); //

private:

    double x1, y1, x2, y2; //箭头的两点坐标


    QPainter *linePainter;
    QList<QLine *> lineList;

    void drawArrow();
    void calcVertexes(double start_x, double start_y, double end_x, double end_y, double& x1, double& y1, double& x2, double& y2);
signals:

public slots:
};

#endif // TOPOLOGYWIDGET_H
