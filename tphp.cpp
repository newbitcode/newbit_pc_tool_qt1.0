#include "tphp.h"


void TPHP_Data::setTemp(int8_t i8Temp)
{
   this->i8Temp = i8Temp;
}

void TPHP_Data::setHumi(uint8_t u8Humi)
{
   this->u8Humi = u8Humi;
}

int8_t TPHP_Data::getTemp(void)
{
 return  this->i8Temp;
}

uint8_t TPHP_Data::getHumi(void)
{
  return this->u8Humi;
}
