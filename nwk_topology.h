#ifndef NWK_TOPOLOGY_H
#define NWK_TOPOLOGY_H



#include <QWidget>
#include "zigbee_data.h"
#include <QLabel>



class NwkTopology: public QLabel, public ZigbeeData
{
public:
       NwkTopology(QWidget *parent=0);
       ~NwkTopology();


private:
    QLabel *nwkAddrLabel;
    QLabel *devTypeLabel;

    int labelHeight;
    int labelWidth;
    QPixmap pix;

    bool bShow = false;


public:
    void set_pixmap(void);
    void show_pixmap(void);
    void newNwkAddrLabel(void);
    void deleteNwkAddrLabel(void);
    void deleteDevTypeLabel(void);
    void newDevTypeLabel(void);

    int getLabelHeight(void);
    int getLabelWidth(void);
    void reset_bShow(void);
    bool get_bShow(void);

    void setData(void);

};







#endif // NWK_TOPOLOGY_H
